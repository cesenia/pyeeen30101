{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# EEEN30101 Numerical Analysis"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Week 01"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "***&copy; 2023 Martínez Ceseña — University of Manchester, UK***"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This notebook introduces the three topics below:\n",
    "- Catastrophic cancellation\n",
    "- Computational efficiency\n",
    "- Heron's algorithm\n",
    "\n",
    "The use of the notebooks is optional and will not be marked. That said, you are strongly encouraged to play with the tools and examples, as you can explore different variations of the examples, which will better prepare you for the exams."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## List of contents"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- [Catastrophic cancellation](#Catastrophic-cancellation)\n",
    "  - [Illustrative example of catastrophic cancellation](#Illustrative-example-of-catastrophic-cancellation)\n",
    "  - [Can we use a different estimation?](#Can-we-use-a-different-estimation?)\n",
    "  - [Which estimation should we use?](#Which-estimation-should-we-use?)\n",
    "\n",
    "\n",
    "- [Computational efficiency](#Computational-efficiency)\n",
    "  - [Multiplying vectors and matrices](#Multiplying-vectors-and-matrices)\n",
    "  - [More efficient calculations](#More-efficient-calculations)\n",
    "\n",
    "\n",
    "- [Heron's algorithm](#Heron's-algorithm)\n",
    "\n",
    "\n",
    "- [Conclusion](#Conclusion)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Before we begin"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before we begin: \n",
    "- Make sure to review the asynchronous materials provided in blackboard for EEEN30101 Week 1 \n",
    "- If you have any questions, please post them in the discussion boards or, if that is not possible, send an email to alex.martinezcesena@manchester.ac.uk"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This notebook provides some examples in python, for that purpose the following libraries will be loaded:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import math  # To use mathematical operation\n",
    "import numpy  # To define and use matrices\n",
    "\n",
    "# To create an interface to the code\n",
    "import ipywidgets as widgets\n",
    "from ipywidgets import interact"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#EEEN30101-Numerical-Analysis)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Catastrophic cancellation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Catastrophic cancellation is a phenomenon where **small** errors in approximated values (e.g., due to rounding) can magnify and become **big** when the values are subtracted."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Illustrative example of catastrophic cancellation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To illustrate this phenomenon, consider the following quadratic equation:\n",
    "\n",
    "$$ax^2+bx+c=0$$\n",
    "\n",
    "For convenience, let us assume our parameters take the following values:\n",
    "\n",
    "$$a=1$$\n",
    "$$b=-2p$$\n",
    "$$c=q$$\n",
    "\n",
    "As a result, our quadratic equation becomes:\n",
    "\n",
    "$$x^2-2x+q=0$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us try to solve this problem using the general quadratic formula:\n",
    "\n",
    "$$\n",
    "\\begin{aligned}\n",
    "  x & = \\frac{-b \\pm \\sqrt{b^2-4ac}}{2a} \\\\\n",
    "   & = \\frac{2p \\pm \\sqrt(4p^2-4q)}{2} \\\\ \n",
    "   & = \\frac{2p \\pm 2\\sqrt(p^2-q)}{2} \\\\\n",
    "   & = p \\pm \\sqrt{p^2-q}\n",
    "\\end{aligned}\n",
    "$$\n",
    "\n",
    "\n",
    "Let us take a step further and define:\n",
    "\n",
    "$$r = \\sqrt{p^2-q}$$\n",
    "\n",
    "As a result, the two solutions of the general quadratic formula are now:\n",
    "\n",
    "$$x_1 = p + r$$\n",
    "$$x_2 = p - r$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And we can code these equations in the python method below. This will allow us to produce estimates of $r$ which will have **small** error. These error can become **big** when we subtract $r$ from $p$.\n",
    "\n",
    "To illustrate this effect, try different values of $p$ and see the results (e.g., between 1 and 10e7):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "0609787080e8459ea86e1a09af8421d9",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(FloatText(value=1000.0, description='p'), FloatText(value=1.0, description='q'), Output(…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "a = widgets.FloatText()\n",
    "\n",
    "@interact\n",
    "def General_Quadratic(p = widgets.FloatText(min=0, value=1000),\n",
    "                      q = widgets.FloatText(min=0, value=1)):\n",
    "    r = math.sqrt(p**2-q)\n",
    "    x1 = p+r\n",
    "    x2 = p-r\n",
    "\n",
    "    print('The estimated value of r  is:', r)\n",
    "    print('The estimated value of x1 is:', x1)\n",
    "    print('The estimated value of x2 is:', x2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From the estimations above, we can make the following observations when $p$ is **big**:\n",
    "- $r$ tends to the same value as $p$:\n",
    "\n",
    "$$r = \\sqrt{p^2-1} \\approx p$$\n",
    "\n",
    "- $x_1$ tends to the value of $2p$:\n",
    "\n",
    "$$x_1 = p+r \\approx 2p$$\n",
    "\n",
    "- $X_2$ tends to zero:\n",
    "\n",
    "$$x_2 = p-r \\approx 0$$\n",
    "\n",
    "><mark>Our estimated value of $x_2$ is bad!</mark>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#EEEN30101-Numerical-Analysis)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Can we use a different estimation?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us retake our original quadratic equation:\n",
    "$$x^2-2x+q=0$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we denote the roots of this equation as $x_1$ and $x_2$ (as we just did above). The equation can be rewritten in factorized form as follows:\n",
    "\n",
    "$$(x-x_1)(x-x_2) = 0$$\n",
    "\n",
    "That is:\n",
    "\n",
    "$$x(x) = x^2 \\text{ (let us ignore this part)}$$\n",
    "\n",
    "$$x(-x_1)+x(-x_2) = -2x \\text{ (let us also ignore this part)}$$\n",
    "\n",
    "$$x_1x_2 = q \\text{ (Lets focus on this part!)}$$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we have set $q=1$ and we are happy with our approximated value of $x_1 \\approx 2p$, we can rewrite the equation as follows:\n",
    "\n",
    "$$x_1x_2 = 1 \\therefore x_2 = \\frac{1}{x1} = \\frac{1}{2p}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us try our new approximation, but this time also try negative values of $p$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "360d7272e26f40829c3cbc49d200117f",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(FloatText(value=1000.0, description='p'), FloatText(value=1.0, description='q'), Output(…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "@interact\n",
    "def General_Quadratic(p = widgets.FloatText(value=1000),\n",
    "                      q = widgets.FloatText(value=1)):\n",
    "    r = math.sqrt(p**2-q)\n",
    "    x1 = p+r\n",
    "    x2 = p-r\n",
    "    nx2 = 1/2/p\n",
    "\n",
    "    print('The estimated value of x1 is    :', x1)\n",
    "    print('The estimated value of x2 is    :', x2)\n",
    "    print('The new estimated value of x2 is:', nx2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "><mark>When $p$ changes its sign, our approximation no longer works for $x_2$, but it seems to match $x_1$</mark>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#EEEN30101-Numerical-Analysis)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Which estimation should we use?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the particular case of the general quadratic formula the following rules are used to avoid catastrophic cancellation:\n",
    "- if $b<0$ (or $p>0$ in our example):\n",
    "\n",
    "$$x_1 = \\frac{-b+\\sqrt{b^2-4ac}}{2a} = p+r$$\n",
    "\n",
    "$$x_2 = \\frac{c}{ax_1}= \\frac{1}{2p}$$\n",
    "\n",
    "- if $b>0$ (or $p<0$ in our example):\n",
    "\n",
    "$$x_1 = \\frac{c}{ax_1}= \\frac{1}{2p}$$\n",
    "\n",
    "$$x_2 = \\frac{-b-\\sqrt{b^2-4ac}}{2a} = p-r$$\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us try the new set of approximations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "ab6d9ed61e214b1c8234646782d8c43e",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(FloatText(value=1000.0, description='p'), FloatText(value=1.0, description='q'), Output(…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "@interact\n",
    "def General_Quadratic(p = widgets.FloatText(value=1000),\n",
    "                      q = widgets.FloatText(value=1)):\n",
    "    r = math.sqrt(p**2-q)\n",
    "    if p>0:\n",
    "        x1 = p+r\n",
    "        x2 = 1/2/p\n",
    "    else:\n",
    "        x1 = 1/2/p\n",
    "        x2 = p-r\n",
    "    \n",
    "    print('The estimated value of x1 is    :', x1)\n",
    "    print('The estimated value of x2 is    :', x2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "><mark>We have now avoided the catastrophic cancellation</mark>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#EEEN30101-Numerical-Analysis)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Computational efficiency"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The availability of computers may encourage us to perform large numbers of complex calculations without thinking much about how to make the calculations more efficient. \n",
    "\n",
    "This can become an issue when you cannot afford to wait long for the result of your calculation (e.g., how to respond to a network issue) or when the problem you are solving is too big and subject to the **curse of dimensionality** (e.g., increasing the size of a problem by 2, will increase calculations and computational time by more than 2)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Multiplying vectors and matrices"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The curse of dimensionality can be illustrated with some vector and matrix calculation.\n",
    "\n",
    "Let us begin by creating two vectors ($u$ and $v$), each with $N$ rows. For the example, let us set $N=10$ and fill the vectors with ones."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "My vector u looks like this:\n",
      " [[1.]\n",
      " [1.]\n",
      " [1.]\n",
      " [1.]\n",
      " [1.]\n",
      " [1.]\n",
      " [1.]\n",
      " [1.]\n",
      " [1.]\n",
      " [1.]]\n",
      "My vector v looks like this:\n",
      " [[1.]\n",
      " [1.]\n",
      " [1.]\n",
      " [1.]\n",
      " [1.]\n",
      " [1.]\n",
      " [1.]\n",
      " [1.]\n",
      " [1.]\n",
      " [1.]]\n"
     ]
    }
   ],
   "source": [
    "N = 10\n",
    "u = numpy.ones((N,1))\n",
    "v = numpy.ones((N,1))\n",
    "\n",
    "print('My vector u looks like this:\\n',u)\n",
    "print('My vector v looks like this:\\n',v)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we are to multiply these vectors, the procedure is:\n",
    "$$u^Tv = \\sum_{n=1}^{n=N}{u_nv_n}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us do the calculation and count the number of computations, also known as flops (floating point operations). For convenience, this procedure is coded as a python method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "This vector multiplication took 19 flops\n",
      "This is equivalent to 2N-1 (19)\n"
     ]
    }
   ],
   "source": [
    "def multiplyVectors(u, v):\n",
    "    N=len(u)\n",
    "    flops = 0\n",
    "    for n in range(N):\n",
    "        multiplication = u[n]*v[n]\n",
    "        flops += 1  # counting number of multiplications\n",
    "    \n",
    "        if n==0: #If it is the same value we keep it\n",
    "            Result = multiplication\n",
    "\n",
    "        else:  # Otherwise, we sum it\n",
    "            Result += multiplication\n",
    "            flops += 1  # counting number of summations\n",
    "    return Result, flops\n",
    "\n",
    "Result, flops = multiplyVectors(u, v)\n",
    "print('This vector multiplication took %d flops'%flops)\n",
    "print('This is equivalent to 2N-1 (%d)'%(2*N-1))\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The number of flops can increase significantly as we begin using larger and larger vectors or matrices. Let us illustrate this by multiplying our vector (effectively a matrix with a single row) with a NxN matrix by using the following formula:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$C_{rowA,colB} = \\sum_{n=1}^{n=N}({A_{rowA,n}B_{n,colB}})   \\;\\;\\; \\forall_{rowA,colB}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once again, for convenience, the procedure is coded as a python method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "This multiplication took 190 flops\n",
      "This is equivalent to 2N^2-N (190)\n"
     ]
    }
   ],
   "source": [
    "def multiplyMatrices(A, B):\n",
    "    flops = 0;\n",
    "    \n",
    "    rowsA = len(A)\n",
    "    colsA = len(A[0])\n",
    "    \n",
    "    rowsB = len(B)\n",
    "    colsB = len(B[0])\n",
    "    \n",
    "    if colsA !=rowsB:\n",
    "        print('A %dx%d matrix cannot be multiplied by a %dx%d matrix'%(rowsA, colsA, rowsB, colsB))\n",
    "        C = 0\n",
    "    else:\n",
    "        C = numpy.zeros((rowsA, colsB))\n",
    "        for ra in range(rowsA):\n",
    "            for cb in range(colsB):\n",
    "                val, flp = multiplyVectors(A[ra,:], B[:, cb])\n",
    "                flops += flp\n",
    "                C[ra, cb] = val\n",
    "\n",
    "    return C, flops\n",
    "\n",
    "v = numpy.ones((1,N))\n",
    "B = numpy.ones((N,N))\n",
    "Result, flops = multiplyMatrices(v, B)\n",
    "\n",
    "print('This multiplication took %d flops'%flops)\n",
    "print('This is equivalent to 2N^2-N (%d)'%(2*N**2-N))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us now try multiplying two NxN matrices:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "This multiplication took 1900 flops\n",
      "This is equivalent to 2N^3-N^2 (1900)\n"
     ]
    }
   ],
   "source": [
    "A = numpy.ones((N,N))\n",
    "B = numpy.ones((N,N))\n",
    "Result, flops = multiplyMatrices(A, B)\n",
    "\n",
    "print('This multiplication took %d flops'%flops)\n",
    "print('This is equivalent to 2N^3-N^2 (%d)'%(2*N**3-N**2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now test how the number of flops changes by counting each operation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "b3bf8160996340adbd9e929d2588e9b4",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(FloatSlider(value=10.0, description='Input', min=1.0, step=1.0), Output()), _dom_classes…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "@interact\n",
    "def TestFlops(N = widgets.FloatSlider(min=1, max=100,step=1,value=10, description='Input')):\n",
    "    N = int(N)\n",
    "    print('The following number of flops are needed to')\n",
    "\n",
    "    # Multiplying vectors\n",
    "    u = numpy.ones((N,1))\n",
    "    v = numpy.ones((N,1))\n",
    "    Result, flops = multiplyVectors(u, v)\n",
    "    print('Multipy two vectors with %3.0f elements:            %10.0f'%(N, flops))\n",
    "\n",
    "    # Multiplying vector and matrix\n",
    "    v = numpy.ones((1,N))\n",
    "    B = numpy.ones((N,N))\n",
    "    Result, flops = multiplyMatrices(v, B)\n",
    "    print('Multipy a %3.0f elements vector and %3.0fx%3.0f matrix: %10.0f'%(N,N,N, flops))\n",
    "\n",
    "    # Multiplying two matrices\n",
    "    A = numpy.ones((N,N))\n",
    "    B = numpy.ones((N,N))\n",
    "    Result, flops = multiplyMatrices(A, B)\n",
    "    print('Multipy two %3.0fx%3.0f matrices:                     %10.0f'%(N,N, flops))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You may notice that the computer may slow down when counting the flops for big matrix operations. In such cases, it is convenient to use more efficient calculations:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "af3b57fc62814c09b5f08d94530cf9c9",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(FloatSlider(value=10.0, description='Input', min=1.0, step=1.0), Output()), _dom_classes…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "@interact\n",
    "def TestFlops(N = widgets.FloatSlider(min=1, max=100,step=1,value=10, description='Input')):\n",
    "    N = int(N)\n",
    "    print('The following number of flops are needed to')\n",
    "    \n",
    "    # Multiplying vectors\n",
    "    flops = 2*N-1\n",
    "    print('Multipy two vectors with %3.0f elements:            %10.0f'%(N, flops))\n",
    "\n",
    "    # Multiplying vector and matrix\n",
    "    flops = 2*N**2-N\n",
    "    print('Multipy a %3.0f elements vector and %3.0fx%3.0f matrix: %10.0f'%(N,N,N, flops))\n",
    "\n",
    "    # Multiplying two matrices\n",
    "    flops = 2*N**3-N**2\n",
    "    print('Multipy two %3.0fx%3.0f matrices:                     %10.0f'%(N,N, flops))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#EEEN30101-Numerical-Analysis)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### More efficient calculations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Even with the currently powerful computers that we have at our disposal, some calculations can become time consuming due to the curse of dimensionality.\n",
    "\n",
    "As a result, it is convenient to explore if there are more cost-effective options to solve problems.\n",
    "\n",
    "For example, consider the following problem (multiplication of two matrices and a vector):"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$x=ABv$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, consider the following two approaches to solve the problem, both leading to the same result but with different number of flops:\n",
    "- Option 1:\n",
    "$$C = AB \\;\\;\\; \\text{  ( this will take $2N^3-N^2$ flops)}$$\n",
    "$$x = Cv \\;\\;\\; \\text{  ( this will take $2N^2-N$ flops)}$$\n",
    "- Option 2:\n",
    "$$y = Bv \\;\\;\\; \\text{  ( this will take $2N^2-N$ flops)}$$\n",
    "$$x = Ay \\;\\;\\; \\text{  ( this will take $2N^2-N$ flops)}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us test the impacts of using these two approaches for matrices of different sizes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "857ae41a57c54e6e9056a3a640868c30",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(FloatSlider(value=10.0, description='Size', max=10000.0, min=1.0, step=1.0), Output()), …"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "@interact\n",
    "def TestABv(N = widgets.FloatSlider(min=1, max=10000,step=1,value=10, description='Size')):\n",
    "    N = int(N)\n",
    "    flops = 2*N**3+N**2-N\n",
    "    print('Solving this problem with option 1 takes %d flops'%flops)\n",
    "    flops = 4*N**2-2*N\n",
    "    print('Solving this problem with option 2 takes %d flops'%flops)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "><mark>Alternative smarter ways to solve the problems can greatly improve computational efficiency</mark>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#EEEN30101-Numerical-Analysis)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Heron's algorithm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As an introduction to iterative algorithms, we will analyse Heron's algorithm to estimate the square root of a number. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Assume you want to get the square root of a number ($S$), according to Heron's algorithm, if you first make a guess of the value of the square root ($x_k$), you can improve your guess ($x_{k+1}$) by solving the following equation successively (iteratively).\n",
    "\n",
    "$$x_{k+1} = \\frac{x_k+S/x_k}{2}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As our guess continues to improve, the differences between our current guesses ($x_k$) and improved guesses ($x_{k+1}$) should become smaller after each iteration.\n",
    "\n",
    "When the difference between our guesses is smaller than a predefined threshold (e.g., we may be very happy with a solution if the difference is lower than 0.0001), the algorithm has converged.\n",
    "\n",
    "Assuming our algorithm converges (we will explore this further next week), we would like our result to converge to the square root of the selected number ($\\sqrt{S}$). To explore if this is the case, assume our initial guess ($x_k$) converges (i.e., $x_k=x_{k+1}=\\bar{x}$) and solve Heron's algorithm as follows:\n",
    "\n",
    "$$\\bar{x} = \\frac{\\bar{x}+S/\\bar{x}}{2}$$\n",
    "$$2\\bar{x} = \\bar{x}+\\frac{S}{\\bar{x}}$$\n",
    "$$\\bar{x} = \\frac{S}{\\bar{x}}$$\n",
    "$$\\bar{x}^2 = S$$\n",
    "$$\\bar{x} = \\sqrt{S}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Use the python method below to play with the algorithm. Try different numbers, guesses, and thresholds."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "991c5def597247b79c9e111a2407c9d0",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(FloatText(value=10.0, description='Input'), FloatText(value=2.0, description='Guess'), F…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "@interact\n",
    "def Heron(Number=widgets.FloatText(value=10, description='Input'),\n",
    "          Guess=widgets.FloatText(value=2, description='Guess'),\n",
    "          Threshold=widgets.FloatText(min=0.000001, value=0.001, description='Threshold')):\n",
    "    Error = 1000\n",
    "    print('     Input  Estimated     Error:')\n",
    "    print('   number:      root:')\n",
    "    print('%10.4f %10.4f'%(Number, Guess))\n",
    "\n",
    "    iterations = 0\n",
    "    while Error > Threshold:\n",
    "        iterations+=1\n",
    "        NewGuess = (Guess+Number/Guess)/2\n",
    "        Error = abs(NewGuess-Guess)\n",
    "        print('%10.4f %10.4f %10.6f'%(Number, NewGuess, Error))\n",
    "        Guess = NewGuess\n",
    "        \n",
    "        if iterations >=100:\n",
    "            Error = 0\n",
    "    \n",
    "    if iterations <100:\n",
    "        print('Converged after %d iterations'%iterations)\n",
    "    else:\n",
    "        print('Failed to converge after %d iterations'%iterations)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Based on your simulations, you may observe the following:\n",
    "- The number of iterations increases if the guess is too high (compared with the result).\n",
    "- The number of iterations increases if the initial guess is too small (e.g., 0.0001).\n",
    "- The number of iterations decreases with higher thresholds (e.g., less accurate results are accepted).\n",
    "- The result is wrong if the initial guess is negative.\n",
    "- The algorithm fails if the initial guess is zero.\n",
    "- The algorithm fails if $S$ is negative."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#EEEN30101-Numerical-Analysis)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Conclusion"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "At the end of this week's lecture and after going through the notebook, you should have a high level idea about the contents of EEEN30101 Numerical Analysis.\n",
    "\n",
    "You should be familiar with the first three topics of the unit and be able to address the following questions:<br/>\n",
    "- Catastrophic cancellation: <br/>\n",
    "      - What is catastrophic cancellation?<br/>\n",
    "      - How can catastrophic cancellation be mitigated or avoided?<br/><br/>\n",
    "- Computational efficiency: <br/>\n",
    "      - What are flops?<br/>\n",
    "      - What is the curse of dimensionality?<br/>\n",
    "      - How can we improve computational efficiency?<br/><br/>\n",
    "- Heron’s algorithm: <br/>\n",
    "      - What is Heron’s algorithm?<br/>\n",
    "      - What are iterative methods?\n",
    "\n",
    "If you cannot answer these questions, you may want to check again this notebook and the lecture notes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to top](#EEEN30101-Numerical-Analysis)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.4"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "426.667px"
   },
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
